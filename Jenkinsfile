pipeline {
	options {
        buildDiscarder(logRotator(numToKeepStr: '5'))
        skipDefaultCheckout()
        timeout(time: 2, unit: 'HOURS')
    }
    agent {
        label 'DockerIO-3'
    }
	
    environment {
        SONARQUBE_HOME = tool 'Sonar-Scanner'
        SONARQUBE_CREDENTIALS_ID = 'admin-sonartest'
        SONARQUBE_URL = 'https://gbssonartest.edst.ibm.com'
        ARTIFACTORY_CREDENTIALS_ID = credentials('artifactory-token')
        ARTIFACTORY_USER = credentials('artifactory-id')
        ARTIFACTORY_URL = 'gbsartifactory.edst.ibm.com'
        ARTIFACTORY_REPO = 'internet-banking'
        GIT_CREDENTIALS_ID = 'petclinic'
        GIT_URL = 'https://gbsgit.edst.ibm.com/Admin-ProjLib/liberty-demo-pet-clinic/liberty-demo.git'
        OC_TOKEN = credentials('oc-login')
        OC_PROJECT = 'fundtransfer-demo'
        OC_URL = 'https://c115-e.us-south.containers.cloud.ibm.com:32198'
        M2_HOME='/opt/tools/apache-maven-3.8.2'
        JAVA_HOME='/opt/tools/jdk-17.0.5'
    }
    stages {
	    stage('Workspacecleanup') {
            steps {
                cleanWs()
            }
		}
		
       stage('git checkout'){
			steps {
                git credentialsId: "${GIT_CREDENTIALS_ID}", url: "${GIT_URL}", branch: 'test'
            }
        }
		stage('Build'){
		    steps {
                echo 'Building..'
	            sh '''
	                env
    	            #export PATH=$GRADLE_HOME/bin:$PATH
    	            export PATH=$JAVA_HOME/bin:$PATH
    	            export PATH=$M2_HOME/bin:$PATH
                    #mvn clean install
                    mvn package -DskipTests -X
    	          
	         
	            '''
	          
            }
		}
		
		stage('Unit/Integration Test'){
		    steps {
                echo 'Building..'
	            sh '''
	                
    	            #export PATH=$GRADLE_HOME/bin:$PATH
    	            export PATH=$JAVA_HOME/bin:$PATH
    	            export PATH=$M2_HOME/bin:$PATH
                    #mvn clean install
                    mvn test
    	          
	         
	            '''
	          
            }
		}
		
        stage ('SonarQube Analysis'){
            steps {
                withSonarQubeEnv('GBSTESTSONAR') {
                sh "${SONARQUBE_HOME}/bin/sonar-scanner " +
                    " -Dsonar.host.url=${SONARQUBE_URL} "  +
                    "  -Dsonar.login=squ_efc8b6eef17c3312b4074895c61c007d49a6599e " +
                    "  -Dsonar.projectKey=Spring-Petclinic " +
                    "  -Dsonar.projectName=Spring-Petclinic " +
                    "  -Dsonar.projectVersion=${BUILD_NUMBER}" +
                    "  -Dsonar.sources=. " +
                    "  -Dsonar.java.binaries=.* " +
                    "  -Dsonar.verbose=true " +
                    "  -Dsonar.exclusions=**/**/dummy.xml "
                }
            }
        }
        
        
        stage('Docker Build') {
            steps {
                sh '''
                    export PATH=$JAVA_HOME/bin:$PATH
                    cd ${WORKSPACE}/spring-petclinic-main
                    docker build -f Dockerfile -t ${ARTIFACTORY_URL}/${ARTIFACTORY_REPO}/pet-clinic:${BUILD_NUMBER} .
      	        '''
            }
        }
        stage('Artifactory Image Push') {
            steps {
                sh '''
                    sleep 10
                    docker login ${ARTIFACTORY_URL} -u ${ARTIFACTORY_USER} -p ${ARTIFACTORY_CREDENTIALS_ID}
                    echo "image push"
                    docker push ${ARTIFACTORY_URL}/${ARTIFACTORY_REPO}/pet-clinic:${BUILD_NUMBER}
                '''
                }
            }
            stage ('UCD deploy') {
                steps {
                    echo 'started deploying in UCD'
                    step([  $class: 'UCDeployPublisher',
                    siteName: 'IBM GBS UCD',
                    component: [
                    $class: 'com.urbancode.jenkins.plugins.ucdeploy.VersionHelper$VersionBlock',
                    componentName: 'pet-clinic',
                    delivery: [
                    $class: 'com.urbancode.jenkins.plugins.ucdeploy.DeliveryHelper$Push',
                    pushVersion: '${BUILD_NUMBER}',
                    baseDir: 'workspace/Internet_Banking/demo_pipeline',
                             ]
                              ],
                    deploy: [
                 $class: 'com.urbancode.jenkins.plugins.ucdeploy.DeployHelper$DeployBlock',
                 deployApp: 'Liberty-demo',
                 deployEnv: 'Dev',
                 deployProc: 'deploy-petclinic',
                 deployVersions: 'pet-clinic:${BUILD_NUMBER}',
                 deployOnlyChanged: false
                         ]
                          ])
                }
             }
    }
        post {
          success {
                slackSend channel: 'slack-notify2',
                teamDomain: 'devsecops-ibm',
                token: "7zSOCsE9wOKIyQGzmTIiUw6F",
                color: 'good',
                message: ":dancer: ${env.JOB_NAME} #${env.BUILD_NUMBER} ${env.BUILD_URL}"
            }
          failure {
                slackSend channel: 'slack-notify2',
                teamDomain: 'devsecops-ibm',
                token: "7zSOCsE9wOKIyQGzmTIiUw6F",
                color: 'bad',
                message: ":bomb: ${env.JOB_NAME} #${env.BUILD_NUMBER} ${env.BUILD_URL}"
            }
          aborted {
                slackSend channel: 'slack-notify2',
                teamDomain: 'devsecops-ibm',
                token: "7zSOCsE9wOKIyQGzmTIiUw6F",
                color: 'red',
                message: ":bomb: ${env.JOB_NAME} #${env.BUILD_NUMBER} ${env.BUILD_URL}"
            }
        }
}
